﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PG3_Timer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Multi Threading
            //var timer = new System.Timers.Timer();
            //timer.Interval = 1000;
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            //timer.Start();

            // Single Threading
            var timer2 = new System.Windows.Forms.Timer();
            timer2.Interval = 1000;
            timer2.Tick += new EventHandler(timer_Elapsed2);
            timer2.Start();
        }

        private void timer_Elapsed(object sender, EventArgs e)
        {
            DateTime utc = DateTime.UtcNow;
            DateTime seoul = utc.AddHours(9.0);
            DateTime la = utc.AddHours(16.0);
            DateTime sydeny = utc.AddHours(11.0);

            Action UpdateTimeLabel = () => 
            {
                LondonTimeLabel.Text = utc.ToLongTimeString();
                SeoulTimeLabel.Text = seoul.ToLongTimeString();
                LATimeLabel.Text = la.ToLongTimeString();
                SydneyTimeLabel.Text = sydeny.ToLongTimeString();
            };

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(UpdateTimeLabel));
            }
            else
            {
                UpdateTimeLabel();
            }
            //UpdateTimeLabel();
        }

        private void timer_Elapsed2(object sender, EventArgs e)
        {
            DateTime utc = DateTime.UtcNow;
            DateTime seoul = utc.AddHours(9.0);
            DateTime la = utc.AddHours(16.0);
            DateTime sydeny = utc.AddHours(11.0);

            LondonTimeLabel.Text = utc.ToLongTimeString();
            SeoulTimeLabel.Text = seoul.ToLongTimeString();
            LATimeLabel.Text = la.ToLongTimeString();
            SydneyTimeLabel.Text = sydeny.ToLongTimeString();
        }
    }
}
