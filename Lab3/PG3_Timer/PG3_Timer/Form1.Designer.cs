﻿namespace PG3_Timer
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.SeoulTimeLabel = new System.Windows.Forms.Label();
            this.LondonTimeLabel = new System.Windows.Forms.Label();
            this.LATimeLabel = new System.Windows.Forms.Label();
            this.SydneyTimeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SeoulTimeLabel
            // 
            this.SeoulTimeLabel.AutoSize = true;
            this.SeoulTimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.SeoulTimeLabel.ForeColor = System.Drawing.Color.Red;
            this.SeoulTimeLabel.Location = new System.Drawing.Point(487, 159);
            this.SeoulTimeLabel.Name = "SeoulTimeLabel";
            this.SeoulTimeLabel.Size = new System.Drawing.Size(70, 12);
            this.SeoulTimeLabel.TabIndex = 0;
            this.SeoulTimeLabel.Text = "Seoul Time";
            // 
            // LondonTimeLabel
            // 
            this.LondonTimeLabel.AutoSize = true;
            this.LondonTimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.LondonTimeLabel.ForeColor = System.Drawing.Color.Blue;
            this.LondonTimeLabel.Location = new System.Drawing.Point(247, 111);
            this.LondonTimeLabel.Name = "LondonTimeLabel";
            this.LondonTimeLabel.Size = new System.Drawing.Size(80, 12);
            this.LondonTimeLabel.TabIndex = 1;
            this.LondonTimeLabel.Text = "London Time";
            // 
            // LATimeLabel
            // 
            this.LATimeLabel.AutoSize = true;
            this.LATimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.LATimeLabel.ForeColor = System.Drawing.Color.Blue;
            this.LATimeLabel.Location = new System.Drawing.Point(76, 197);
            this.LATimeLabel.Name = "LATimeLabel";
            this.LATimeLabel.Size = new System.Drawing.Size(53, 12);
            this.LATimeLabel.TabIndex = 2;
            this.LATimeLabel.Text = "LA Time";
            // 
            // SydneyTimeLabel
            // 
            this.SydneyTimeLabel.AutoSize = true;
            this.SydneyTimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.SydneyTimeLabel.ForeColor = System.Drawing.Color.Blue;
            this.SydneyTimeLabel.Location = new System.Drawing.Point(522, 291);
            this.SydneyTimeLabel.Name = "SydneyTimeLabel";
            this.SydneyTimeLabel.Size = new System.Drawing.Size(81, 12);
            this.SydneyTimeLabel.TabIndex = 3;
            this.SydneyTimeLabel.Text = "Sydney Time";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PG3_Timer.Properties.Resources.map;
            this.ClientSize = new System.Drawing.Size(644, 381);
            this.Controls.Add(this.SydneyTimeLabel);
            this.Controls.Add(this.LATimeLabel);
            this.Controls.Add(this.LondonTimeLabel);
            this.Controls.Add(this.SeoulTimeLabel);
            this.Name = "Form1";
            this.Text = "세계의 현지 시각";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SeoulTimeLabel;
        private System.Windows.Forms.Label LondonTimeLabel;
        private System.Windows.Forms.Label LATimeLabel;
        private System.Windows.Forms.Label SydneyTimeLabel;
    }
}

