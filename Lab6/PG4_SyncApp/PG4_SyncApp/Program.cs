﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PG4_SyncApp
{
    class Program
    {
        private static DateTime startTime;
        private static object lockObj = new object();
        private static Random R = new Random();
        static void Main(string[] args)
        {
            Queue<int> queue = new Queue<int>();

            Thread printThread = new Thread(new ParameterizedThreadStart(PrintValue));
            printThread.IsBackground = true;
            printThread.Start(queue);

            int count = 0;
            startTime = DateTime.Now;
            while ((DateTime.Now - startTime).TotalSeconds < 180)
            {
                lock (lockObj)
                {
                    queue.Enqueue(count++);
                }
                Thread.Sleep(1000);
            }


        }
        private static void PrintValue(object queue)
        {
            while (true)
            {
                var sleepTime = R.Next(1, 10);
                Thread.Sleep(sleepTime * 1000);

                string allValue = "(" + (DateTime.Now - startTime).TotalSeconds + ") ";
                lock (lockObj)
                {
                    foreach (var value in (Queue<int>)queue)
                    {
                        allValue += value + " ";
                    }
                }
                Console.WriteLine(allValue);
            }
        }
    }
}
