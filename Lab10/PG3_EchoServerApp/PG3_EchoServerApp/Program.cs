﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace PG3_EchoServerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, 5000);
            sock.Bind(ep);
            sock.Listen(100);

            Socket csock = sock.Accept();

            byte[] buff = new byte[8192];
            while (!Console.KeyAvailable)
            {
                int n = csock.Receive(buff);
                string data = Encoding.UTF8.GetString(buff, 0, n);
                Console.WriteLine(data);

                byte[] msg = Encoding.UTF8.GetBytes(data);
                csock.Send(msg, 0, n, SocketFlags.None);
            }

            csock.Close();
            sock.Close();
        }
    }

}
