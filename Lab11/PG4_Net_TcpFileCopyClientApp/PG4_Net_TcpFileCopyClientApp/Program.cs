﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;
namespace PG4_Net_TcpFileCopyClientApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const int BUFF_SIZE = 1024;
            // (1) IP 주소와 포트를 지정하고 TCP 연결
            TcpClient tc = new TcpClient("127.0.0.1", 7000);

            string filePath = string.Empty;
            filePath = @"D:\Desktop\2020-10-29-SPS+도어폼.mp4";
            // (2) NetworkStream을 얻어옴
            NetworkStream stream = tc.GetStream();
            //while((filePath = Console.ReadLine()) != "Q")
            {
                
                
                // (3) File 
                var fileBuffer = ReadFileBytes(filePath);

                long totalBytes = fileBuffer.Length;
                int currentPos = 0;
                byte[] sendBuffer = new byte[BUFF_SIZE];
                while (totalBytes > currentPos)
                {
                    currentPos += BUFF_SIZE;
                    Array.Copy(fileBuffer, currentPos, sendBuffer, 0, BUFF_SIZE);
                    stream.Write(sendBuffer, 0, sendBuffer.Length);
                }
            }
            stream.Close();
            tc.Close();
        }

        public static byte[] ReadFileBytes(string filePath)
        {
            FileInfo fi = new FileInfo(filePath);
            byte[] fileNameSize = BitConverter.GetBytes(fi.Name.Length);
            byte[] fileName = Encoding.UTF8.GetBytes(fi.Name);
            byte[] fileSize = File.ReadAllBytes(filePath);
            byte[] file = BitConverter.GetBytes(fileSize.Length);
            byte[] fileBuffer = new byte[fileNameSize.Length + fileName.Length + fileSize.Length + file.Length];

            int index = 0;
            Action<byte[]> CopyToSendBuffer = (array) =>
            {
                array.CopyTo(fileBuffer, index);
                index += array.Length;
            };

            CopyToSendBuffer(fileNameSize);
            CopyToSendBuffer(fileName);
            CopyToSendBuffer(fileSize);
            CopyToSendBuffer(file);

            return fileBuffer;
        }

        public static void OtherMethod(string filePath)
        {

        }
    }
}
