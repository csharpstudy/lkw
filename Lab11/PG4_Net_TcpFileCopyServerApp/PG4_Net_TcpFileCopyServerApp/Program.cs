﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace PG4_Net_TcpFileCopyServerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncEchoServer().Wait();
        }

        async static Task AsyncEchoServer()
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 7000);
            listener.Start();
            while (true)
            {
                // 비동기 Accept
                TcpClient tcp = await listener.AcceptTcpClientAsync().ConfigureAwait(false);

                // 새 스레드에서 처리
                Task.Factory.StartNew(AsyncTcpProcess, tcp);
            }
        }

        async static void AsyncTcpProcess(object o)
        {
            TcpClient tcp = (TcpClient)o;

            int MAX_SIZE = 1024; // 가정
            NetworkStream stream = tcp.GetStream();

            // 비동기 수신
            var buff = new byte[MAX_SIZE];
            var nbytes = await stream.ReadAsync(buff, 0, buff.Length).ConfigureAwait(false);
            int receiveSize;
            if (nbytes > 0)
            {
                //receiveSize = 
            }

            stream.Close();
            tcp.Close();
        }
    }
}
