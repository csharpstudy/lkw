﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace PG3_ThreadPoolFileHashApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Directory.GetCurrentDirectory() + "\\Data\\";
            string[] files = Directory.GetFiles(path, "*.txt");
            int filesCount = files.Length;
            foreach (var dataFile in files)
            {
                ThreadPool.QueueUserWorkItem(CreateFileHash, dataFile);
            }

            Console.ReadLine();
        }

        static void CreateFileHash(object filePath)
        {
            // 해시 작업
            byte[] fileBytes = File.ReadAllBytes(filePath.ToString());

            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] hash = sha1.ComputeHash(fileBytes);

            string hexhash = BitConverter.ToString(hash).Replace("-", "").ToLower();
            string[] temp = filePath.ToString().Split(new string[] { "\\" }, StringSplitOptions.None);
            string fileName = temp[temp.Length - 1];
            using (var file = new StreamWriter(filePath + ".sha1"))
            {
                file.WriteLine("{0}  {1}", hexhash, fileName);
            }
            Console.WriteLine("{0}  {1} - Done", hexhash, fileName);
        }
    }
}
