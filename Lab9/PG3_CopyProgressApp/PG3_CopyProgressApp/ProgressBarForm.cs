﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PG3_CopyProgressApp
{
    public partial class ProgressBarForm : Form
    {
        private bool _cancel;

        public ProgressBarForm(string fileName)
        {
            InitializeComponent();
            ProgressTextLabel.Text = fileName + "파일을 복사중입니다.";

            _cancel = false;
        }

        private void ProgressBarForm_Load(object sender, EventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Continuous;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
        }

        public void ChangeProgress(double percentage, ref bool cancel)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => 
                {
                    progressBar1.Value = (int)percentage;
                }));
            }
            else
                progressBar1.Value = (int)percentage;

            cancel = _cancel;
        }

        public void Complete()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => { this.Close(); }));
            }
            else
                this.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            _cancel = true;
        }
    }
}
