﻿namespace PG3_CopyProgressApp
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SourceDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.TargetDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.SourceSearchBtn = new System.Windows.Forms.Button();
            this.TargetSearchBtn = new System.Windows.Forms.Button();
            this.TargetTitleLabel = new System.Windows.Forms.Label();
            this.SourceTitleLabel = new System.Windows.Forms.Label();
            this.SourceListView = new System.Windows.Forms.ListView();
            this.TargetListView = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // SourceDirectoryTextBox
            // 
            this.SourceDirectoryTextBox.Location = new System.Drawing.Point(54, 17);
            this.SourceDirectoryTextBox.Name = "SourceDirectoryTextBox";
            this.SourceDirectoryTextBox.Size = new System.Drawing.Size(164, 21);
            this.SourceDirectoryTextBox.TabIndex = 0;
            // 
            // TargetDirectoryTextBox
            // 
            this.TargetDirectoryTextBox.Location = new System.Drawing.Point(403, 20);
            this.TargetDirectoryTextBox.Name = "TargetDirectoryTextBox";
            this.TargetDirectoryTextBox.Size = new System.Drawing.Size(164, 21);
            this.TargetDirectoryTextBox.TabIndex = 1;
            // 
            // SourceSearchBtn
            // 
            this.SourceSearchBtn.Location = new System.Drawing.Point(224, 17);
            this.SourceSearchBtn.Name = "SourceSearchBtn";
            this.SourceSearchBtn.Size = new System.Drawing.Size(32, 23);
            this.SourceSearchBtn.TabIndex = 2;
            this.SourceSearchBtn.Text = "...";
            this.SourceSearchBtn.UseVisualStyleBackColor = true;
            this.SourceSearchBtn.Click += new System.EventHandler(this.SourceSearchBtn_Click);
            // 
            // TargetSearchBtn
            // 
            this.TargetSearchBtn.Location = new System.Drawing.Point(573, 20);
            this.TargetSearchBtn.Name = "TargetSearchBtn";
            this.TargetSearchBtn.Size = new System.Drawing.Size(32, 23);
            this.TargetSearchBtn.TabIndex = 3;
            this.TargetSearchBtn.Text = "...";
            this.TargetSearchBtn.UseVisualStyleBackColor = true;
            this.TargetSearchBtn.Click += new System.EventHandler(this.TargetSearchBtn_Click);
            // 
            // TargetTitleLabel
            // 
            this.TargetTitleLabel.AutoSize = true;
            this.TargetTitleLabel.Location = new System.Drawing.Point(368, 23);
            this.TargetTitleLabel.Name = "TargetTitleLabel";
            this.TargetTitleLabel.Size = new System.Drawing.Size(29, 12);
            this.TargetTitleLabel.TabIndex = 4;
            this.TargetTitleLabel.Text = "타겟";
            // 
            // SourceTitleLabel
            // 
            this.SourceTitleLabel.AutoSize = true;
            this.SourceTitleLabel.Location = new System.Drawing.Point(19, 20);
            this.SourceTitleLabel.Name = "SourceTitleLabel";
            this.SourceTitleLabel.Size = new System.Drawing.Size(29, 12);
            this.SourceTitleLabel.TabIndex = 5;
            this.SourceTitleLabel.Text = "소스";
            // 
            // SourceListView
            // 
            this.SourceListView.AllowDrop = true;
            this.SourceListView.HideSelection = false;
            this.SourceListView.Location = new System.Drawing.Point(21, 46);
            this.SourceListView.Name = "SourceListView";
            this.SourceListView.Size = new System.Drawing.Size(235, 267);
            this.SourceListView.TabIndex = 6;
            this.SourceListView.UseCompatibleStateImageBehavior = false;
            this.SourceListView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.SourceListView_ItemDrag);
            // 
            // TargetListView
            // 
            this.TargetListView.AllowDrop = true;
            this.TargetListView.HideSelection = false;
            this.TargetListView.Location = new System.Drawing.Point(370, 46);
            this.TargetListView.Name = "TargetListView";
            this.TargetListView.Size = new System.Drawing.Size(235, 267);
            this.TargetListView.TabIndex = 7;
            this.TargetListView.UseCompatibleStateImageBehavior = false;
            this.TargetListView.DragDrop += new System.Windows.Forms.DragEventHandler(this.TargetListView_DragDrop);
            this.TargetListView.DragEnter += new System.Windows.Forms.DragEventHandler(this.TargetListView_DragEnter);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "videoplay32.png");
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PG3_CopyProgressApp.Properties.Resources.dragndrop;
            this.pictureBox1.Location = new System.Drawing.Point(269, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 323);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.TargetListView);
            this.Controls.Add(this.SourceListView);
            this.Controls.Add(this.SourceTitleLabel);
            this.Controls.Add(this.TargetTitleLabel);
            this.Controls.Add(this.TargetSearchBtn);
            this.Controls.Add(this.SourceSearchBtn);
            this.Controls.Add(this.TargetDirectoryTextBox);
            this.Controls.Add(this.SourceDirectoryTextBox);
            this.Name = "Form1";
            this.Text = "파일복사 유틸러티";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox SourceDirectoryTextBox;
        private System.Windows.Forms.TextBox TargetDirectoryTextBox;
        private System.Windows.Forms.Button SourceSearchBtn;
        private System.Windows.Forms.Button TargetSearchBtn;
        private System.Windows.Forms.Label TargetTitleLabel;
        private System.Windows.Forms.Label SourceTitleLabel;
        private System.Windows.Forms.ListView SourceListView;
        private System.Windows.Forms.ListView TargetListView;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

