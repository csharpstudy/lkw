﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PG3_CopyProgressApp
{
    public partial class Form1 : Form
    {
        private enum PATH_TYPE
        {
            SOURCE, 
            TARGET,
        }

        private FileMananger _fileManager;
        private string _sourcePath;
        private string _targetPath;
        public Form1()
        {
            InitializeComponent();
            SourceListView.View = View.LargeIcon;
            SourceListView.LargeImageList = imageList1;
            TargetListView.View = View.LargeIcon;
            TargetListView.LargeImageList = imageList1;

            _fileManager = new FileMananger(this);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ChangePath(PATH_TYPE.SOURCE, @"C:\Source");
            this.ChangePath(PATH_TYPE.TARGET, @"C:\Target");
        }

        private void LoadFolder(ListView listView, string directory)
        {
            DirectoryInfo di = new DirectoryInfo(directory);
            FileInfo[] files = di.GetFiles();

            listView.BeginUpdate();
            listView.Items.Clear();
            foreach (var file in files)
            {
                var newItem = new ListViewItem(file.Name) { Name = file.Name, ImageIndex = 0 };
                listView.Items.Add(newItem);
            }
            listView.EndUpdate();
        }

        private bool BrowseFolder(out string path)
        {
            var fbDialog = new FolderBrowserDialog();
            if (fbDialog.ShowDialog() == DialogResult.OK)
            {
                path = fbDialog.SelectedPath;
                return true;
            }

            path = "";
            return false;
        }


        private void ChangePath(PATH_TYPE type, string directoryPath)
        {
            switch (type)
            {
                case PATH_TYPE.SOURCE:
                    _sourcePath = directoryPath;
                    SourceDirectoryTextBox.Text = _sourcePath;
                    LoadFolder(SourceListView, _sourcePath);
                    break;
                case PATH_TYPE.TARGET:
                    _targetPath = directoryPath;
                    TargetDirectoryTextBox.Text = _targetPath;
                    LoadFolder(TargetListView, _targetPath);
                    break;
            }
            
        }
        private void SourceSearchBtn_Click(object sender, EventArgs e)
        {
            if (BrowseFolder(out string directoryPath))
            {
                this.ChangePath(PATH_TYPE.SOURCE, directoryPath);
            }
        }

        private void TargetSearchBtn_Click(object sender, EventArgs e)
        {
            if (BrowseFolder(out string directoryPath))
            {
                this.ChangePath(PATH_TYPE.TARGET, directoryPath);
            }
        }

        // Drop Source : ItemDrag 이벤트 핸들러에서 DoDragDrop 호출
        private void SourceListView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Copy);
        }

        // Drop Target: DragEnter 핸들러에서 해당 소스가 ListViewItem 인지 체크
        private void TargetListView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        // Drop Target: DragDrop 핸들러에서 복사 실행
        private void TargetListView_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                // 드래그된 소스 ListViewItem
                var item = e.Data.GetData(typeof(ListViewItem)) as ListViewItem;

                _fileManager.CopyFile(_sourcePath, _targetPath, item.Name);
            }
        }

        public void RefresehTargetListView()
        {
            if (TargetListView.InvokeRequired)
            {
                TargetListView.Invoke(new Action(() => { LoadFolder(TargetListView, _targetPath); }));
            }
            else
                LoadFolder(TargetListView, _targetPath);
        }
    }
}
