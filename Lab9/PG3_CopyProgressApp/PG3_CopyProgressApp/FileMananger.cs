﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PG3_CopyProgressApp
{
    public class FileMananger
    {
        public delegate void ProgressChangeDelegate(double persentatge, ref bool cancel);
        public delegate void CompleteDelegate();

        public event ProgressChangeDelegate OnProgressChanged;
        public event CompleteDelegate OnComplete;
        public FileMananger(Form1 mainForm)
        {
            OnProgressChanged = delegate { };
            OnComplete = mainForm.RefresehTargetListView;
        }

        private void CopyStart(string sourceFile, string destFile)
        {
            byte[] buffer = new byte[1024 * 1024]; // 1MB buffer
            bool cancelFlag = false;

            using (FileStream source = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
            {
                long fileLength = source.Length;
                using (FileStream dest = new FileStream(destFile, FileMode.CreateNew, FileAccess.Write))
                {
                    long totalBytes = 0;
                    int currentBlockSize = 0;
                    while ((currentBlockSize = source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        totalBytes += currentBlockSize;
                        double percentage = (double)totalBytes * 100.0 / fileLength;

                        dest.Write(buffer, 0, currentBlockSize);

                        cancelFlag = false;
                        OnProgressChanged(percentage, ref cancelFlag);

                        if (cancelFlag == true)
                        {
                            break;
                        }
                    }
                }

                if (cancelFlag == true)
                    File.Delete(destFile);

                OnComplete();
            }
        }

        public void CopyFile(string sourcePath, string targetPath, string fileName)
        {
            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            if (!File.Exists(destFile))
            {
                var progressFoam = new ProgressBarForm(fileName);
                progressFoam.Show();

                OnProgressChanged += progressFoam.ChangeProgress;
                OnComplete += progressFoam.Complete;

                Thread copyThread = new Thread(() => CopyStart(sourceFile, destFile));
                copyThread.IsBackground = true;
                copyThread.Start();
            }
            else
                System.Windows.Forms.MessageBox.Show("같은 이름의 파일이 이미 존재합니다.");

        }

    }
}
