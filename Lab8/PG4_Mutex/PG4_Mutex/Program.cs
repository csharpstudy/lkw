﻿using System;
using System.Runtime.InteropServices; 


using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace PG4_Mutex
{
    static class Program
    {
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int flags);
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        static readonly string mtxName = "60C3D9CA-5957-41B2-9B6D-419DC9BE77DF";
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew;
            Mutex mtx = new Mutex(true, mtxName, out createdNew);

            if (!createdNew)
            {
                var processes = Process.GetProcessesByName("PG4_Mutex");
                foreach (var p in processes)
                {
                    if (p.Id == Process.GetCurrentProcess().Id)
                        continue;

                    IntPtr handle = p.MainWindowHandle;
                    ShowWindow(handle, 1);
                    SetForegroundWindow(handle);
                }
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
