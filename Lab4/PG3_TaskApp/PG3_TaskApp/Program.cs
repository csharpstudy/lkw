﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PG3_TaskApp
{
    class Program
    {
        public static Random R = new Random();
        static void Main(string[] args)
        {
            Task[] genTasks = new Task[10];
            for (int i = 0; i < 10; i++)
            {
                genTasks[i] = Task.Factory.StartNew((k) =>
                {
                    using (var file = new StreamWriter("random." + k))
                    {
                        for (int j = 0; j < 1000; j++)
                        {
                            file.WriteLine(R.Next());
                        }
                    }
                    Console.WriteLine("random.{0} - Done", k);
                }, i);
            }

            Task.WaitAll(genTasks);

            Task<int>[] maxTasks = new Task<int>[10];
            for (int i = 0; i < 10; i++)
            {
                maxTasks[i] = Task.Factory.StartNew<int>((k) => 
                {
                    int max = int.MinValue;
                    var file = File.ReadAllLines("random." + k);
                    
                    //Console.WriteLine("random." + index + " length : " + file.Length);
                    for (int j = 0; j < file.Length; j++)
                    {
                        int randomNumber = Convert.ToInt32(file[j]);
                        if (randomNumber > max)
                            max = randomNumber;
                    }
                    
                    
                    return max;
                }, i);
            }

            for (int i = 0; i < 10; i++)
            {
                int maxNumber = maxTasks[i].Result;
                Console.WriteLine("random.{0}'s max num : {1}", i, maxNumber);
            }
        }
    }
}
