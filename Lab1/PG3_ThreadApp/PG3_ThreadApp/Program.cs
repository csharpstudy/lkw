﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace PG3_ThreadApp
{
    class Program
    {
        private const int INDEX_ID = 0;
        private const int INDEX_KOREAN = 1;
        private const int INDEX_ENGLISH = 2;
        private const int INDEX_MATH = 3;
        static void Main(string[] args)
        {
            var path = @"..\";
            path = Directory.GetCurrentDirectory() + @"\..\..\..\..\";
            string[] files = Directory.GetFiles(path, "*.txt");
            int filesCount = files.Length;
            foreach (var dataFile in files)
            {
                //Thread th = new Thread(() => Calculate(dataFile));
                //th.Start();

                Thread th = new Thread(new ParameterizedThreadStart(Calculate));
                th.Start(dataFile);
            }

            // 아래 코드의 경우 Index Exception이 발생합니다. 원인은 잘 모르겠습니다.
            // --> Closure의 영향
            //for (int i = 0; i < filesCount; i++)
            //{
            //    int j = i;
            //    Thread th = new Thread(() => 
            //    {
            //        Console.WriteLine(j);
            //        Calculate(files[j]);
            //    }); 
            //    th.Start();
            //}

            Console.ReadLine();
        }

        // dataFile라는 파라메터를 object 타입으로 받아들여야함
        public static void Calculate(object dataFile)
        {
            var outputs = new List<string>();
            var file = File.ReadAllLines((string)dataFile);
            for (int i = 0; i < file.Length; i++)
            {
                var values = file[i].Split(',').ToList();
                double koreanScore = Convert.ToDouble(values[INDEX_KOREAN]);
                double englishScore = Convert.ToDouble(values[INDEX_ENGLISH]);
                double mathScore = Convert.ToDouble(values[INDEX_MATH]);
                double sum = koreanScore + englishScore + mathScore;
                double avg = sum / 3;
                var output = values[INDEX_ID] + ", " + koreanScore + ", " + englishScore + ", " + mathScore + ", " + sum + ", " + avg;
                outputs.Add(output);
            }

            File.WriteAllLines(dataFile + ".OUT", outputs);
            Console.WriteLine("{0}: {1} - Done", DateTime.Now, dataFile);
        }

    }
}
