﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PG4_SyncEventApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var generateTasks = new Task[10];
            Manager manager = new Manager();
            ThreadPool.SetMinThreads(10, 10); // 최소 스레드풀 크기 증가
            for (int i = 0; i < 10; i++)
            {
                int id = i;
                generateTasks[i] = Task.Factory.StartNew(() => { (new DeliveryMan(id, manager.AutoEvent)).Deliver(); });
            }

            Thread.Sleep(1000); // 약간의 Delay

            int count = 1;
            while (count <= 20) // 20번의 Delivery 시뮬레이트
            {
                manager.Ring();
                Thread.Sleep(100); 
                count++;
            }
        }            
    }
}
