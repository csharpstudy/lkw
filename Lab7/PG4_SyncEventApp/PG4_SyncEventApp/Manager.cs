﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PG4_SyncEventApp
{
    public class Manager
    {
        private AutoResetEvent _autoEvent;

        public AutoResetEvent AutoEvent { get { return _autoEvent; } }

        public Manager()
        {
            _autoEvent = new AutoResetEvent(false);
        }

        public void Ring()
        {
            _autoEvent.Set();
        }
    }
}
