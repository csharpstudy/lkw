﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PG4_SyncEventApp
{
    public class DeliveryMan
    {
        private int _id;
        private AutoResetEvent _managerAutoEvent;

        public DeliveryMan(int id, AutoResetEvent managerAutoEvent)
        {
            _id = id;
            _managerAutoEvent = managerAutoEvent;
        }

        public void Deliver()
        {
            while (true)
            {
                _managerAutoEvent.WaitOne();

                Console.WriteLine("{0} : {1}", _id, DateTime.Now.ToLocalTime());

                Thread.Sleep(1000); // 1초 대기
            }
        }
    }
}
