﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLibrary;

namespace PG4_AwaitApp
{
    public partial class Form1 : Form
    {
        DataManager _dataManager = new DataManager();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadOrderList();
            LoadTotalCost();
            LoadTotalCount();
        }

        private async void LoadOrderList()
        {
            Task<List<Order>> loadLastOrderTask = Task<List<Order>>.Factory.StartNew(() => _dataManager.GetLast5Orders());

            await loadLastOrderTask;

            OrderListDataGridView.DataSource = loadLastOrderTask.Result;
        }

        private async void LoadTotalCost()
        {
            Task<decimal> loadTotalCostTask = Task<decimal>.Factory.StartNew(() => _dataManager.GetOrderTotal());

            await loadTotalCostTask;

            TotalCostText.Text = loadTotalCostTask.Result.ToString();
        }

        private async void LoadTotalCount()
        {
            Task<decimal> loadTotalCountTask = Task<decimal>.Factory.StartNew(() => _dataManager.GetOrderBookCount());

            await loadTotalCountTask;

            TotalCountText.Text = loadTotalCountTask.Result.ToString();

        }
    }
}
