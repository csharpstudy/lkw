﻿namespace PG4_AwaitApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrderListDataGridView = new System.Windows.Forms.DataGridView();
            this.TotalCostText = new System.Windows.Forms.TextBox();
            this.TotalCountText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.OrderListDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // OrderListDataGridView
            // 
            this.OrderListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderListDataGridView.Location = new System.Drawing.Point(22, 48);
            this.OrderListDataGridView.Name = "OrderListDataGridView";
            this.OrderListDataGridView.RowTemplate.Height = 23;
            this.OrderListDataGridView.Size = new System.Drawing.Size(281, 179);
            this.OrderListDataGridView.TabIndex = 0;
            // 
            // TotalCostText
            // 
            this.TotalCostText.Font = new System.Drawing.Font("굴림", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.TotalCostText.Location = new System.Drawing.Point(350, 48);
            this.TotalCostText.Name = "TotalCostText";
            this.TotalCostText.Size = new System.Drawing.Size(160, 63);
            this.TotalCostText.TabIndex = 1;
            // 
            // TotalCountText
            // 
            this.TotalCountText.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.TotalCountText.Location = new System.Drawing.Point(350, 162);
            this.TotalCountText.Name = "TotalCountText";
            this.TotalCountText.Size = new System.Drawing.Size(160, 50);
            this.TotalCountText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(348, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "주문 총액";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(348, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "주문 총 수량";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "최근 주문 리스트";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 255);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TotalCountText);
            this.Controls.Add(this.TotalCostText);
            this.Controls.Add(this.OrderListDataGridView);
            this.Name = "Form1";
            this.Text = "주문 대시보드";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OrderListDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView OrderListDataGridView;
        private System.Windows.Forms.TextBox TotalCostText;
        private System.Windows.Forms.TextBox TotalCountText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}